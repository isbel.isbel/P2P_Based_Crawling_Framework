﻿using AggregatorFinal;
using AggregatorFinal.DataAggregator;
using AggregatorFinal.Rec;
using ConsoleApplication6;
using ConsoleApplication6.SecondPhase;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AgrigateData
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryInfo terget = Directory.CreateDirectory(Aggregator.targetFolder);
            Directory.CreateDirectory(Aggregator.resultFolder);
            DirectoryInfo report = Directory.CreateDirectory(Aggregator.reportFolder);

            Console.WriteLine("Enter: LocalIP LocalPort");
            
            String temp = Console.ReadLine();
            char[] splitter = { ' ' };
            String[] parts = temp.Split(splitter, StringSplitOptions.None);
            string port = parts[1];
            string ip = parts[0];

            new MainThread(ip, port);

            Console.WriteLine("Opt 1: 'ag' to aggregate");
            Console.WriteLine("Opt 2: 'report1' to get hourly report");
            

            while (true)
            {
                temp = Console.ReadLine();
                if (temp == "report1")
                {
                    SecondPhase.gethoursDiagramSepPortsOnline(terget.FullName, report.FullName + "/16471Online.txt", report.FullName + "/16470Online.txt");
                }
                if (temp == "ag")
                {
                    new Aggregator();
                }
            }

        }



    }
}
