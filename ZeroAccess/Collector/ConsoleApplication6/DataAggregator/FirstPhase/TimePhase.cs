﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgrigateData
{
    class TimePhrase
    {
        public DateTime datetime;
        public List<Item> list = new List<Item>();

        public TimePhrase(DateTime dTime)
        {
            datetime = new DateTime(dTime.Year, dTime.Month, dTime.Day, dTime.Hour, dTime.Minute, dTime.Second);
        }

        public static List<Item> SortAscending(List<Item> list)
        {
            list.Sort((a, b) => a.datetime.CompareTo(b.datetime));
            return list;
        }
    }
}
