﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace AggregatorFinal
{
    class UDP_Sender
    {
        public Socket _socket;
        IPEndPoint _ipEndPoint;

        public UDP_Sender(string ip, string port)
        {
            _ipEndPoint = new IPEndPoint(IPAddress.Parse(ip.Trim()), Convert.ToInt32(port.Trim()));
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            _socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveBuffer, 0);
        }

        public void sendMessage(string text)
        {
            byte[] data = Encoding.ASCII.GetBytes(text);
            _socket.SendTo(data, data.Length, SocketFlags.None, _ipEndPoint);

        }

        public static void forwardMessage(string ip, string port, string message)
        {

            try
            {
                UDP_Sender udp_Sender = new UDP_Sender(ip, port);
                udp_Sender.sendMessage(message);
                udp_Sender._socket.Close();
            }
            catch
            {

            }
        }
    }
}
